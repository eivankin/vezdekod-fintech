import os
from brownie import SecondTask, accounts
from dotenv import load_dotenv

load_dotenv()


def main():
    acct = accounts.add(os.getenv('PRIV_KEY'))
    SecondTask.deploy(acct, {'from': acct}, publish_source=True)
