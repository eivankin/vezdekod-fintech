import pytest
import brownie
from brownie import accounts, SecondTask


@pytest.fixture
def owner():
    return accounts[0]


@pytest.fixture
def contract(owner):
    return SecondTask.deploy(owner, {'from': owner})


def test_balance(contract, owner):
    assert contract.balanceOf(owner) == 0


def test_mint(contract, owner):
    recipient = accounts[0]
    tx = contract.mint(recipient, 100, {'from': owner})
    assert contract.balanceOf(recipient) == 100
    assert 'Transfer' in tx.events


def test_mint_not_from_owner(contract):
    with brownie.reverts('This method is only for contract owner'):
        contract.mint(accounts[1], 100, {'from': accounts[1]})


def test_burn(contract, owner):
    recipient = accounts[0]
    contract.mint(recipient, 100, {'from': owner})
    tx = contract.burn(100, {'from': recipient})
    assert contract.balanceOf(recipient) == 0
    assert 'Transfer' in tx.events


def test_burn_exceeds(contract, owner):
    recipient = accounts[0]
    contract.mint(recipient, 100, {'from': owner})
    with brownie.reverts('ERC20: burn amount exceeds balance'):
        contract.burn(101, {'from': recipient})


def test_total_supply(contract, owner):
    recipient = accounts[0]
    contract.mint(recipient, 100, {'from': owner})
    assert contract.totalSupply() == 100
    contract.burn(100, {'from': recipient})
    assert contract.totalSupply() == 0


def test_transfer(contract, owner):
    recipient = accounts[1]
    contract.mint(owner, 100, {'from': owner})
    tx = contract.transfer(recipient, 100, {'from': owner})
    assert contract.balanceOf(recipient) == 100
    assert contract.balanceOf(owner) == 0
    assert 'Transfer' in tx.events


def test_approve_and_allowance(contract, owner):
    spender = accounts[1]
    assert contract.allowance(owner, spender) == 0
    tx = contract.approve(spender, 100, {'from': owner})
    assert contract.allowance(owner, spender) == 100
    assert 'Approval' in tx.events


def test_transfer_from(contract, owner):
    recipient = accounts[1]
    sender = accounts[2]
    contract.approve(owner, 100, {'from': sender})
    contract.mint(sender, 100, {'from': owner})
    tx = contract.transferFrom(sender, recipient, 100, {'from': owner})
    assert contract.balanceOf(recipient) == 100
    assert 'Transfer' in tx.events
